//   Função são principais, sao importantes no programa JS 

//   Existem formas de criar uma função
//   -> Forma literal:
function fun1(){
    console.log("ola")
}
fun1()

//  -> Armazenar em uma variavel:
let fun2 = function(){
      console.log("bom dia")
   }
fun2()


//  -> Armazenar  em um Array
let array = [function(a,b){ return a + b}, fun1, fun2]
console.log(array[0](4,5))
array[1]()
array[2]()



// -> Armazenar em um Atributo de um Objeto
let pessoa = {
    nome: "joao",
    falar: function(){ console.log("opa sksskks")}
}
pessoa.falar()



// -> Passar função como parametro
function run(fun){
  try{
    fun()
  }
  catch{
    throw "o param passado Nao e uma funçao"
  }
}
run(function(){console.log("tudo ok?")})




// Uma função pode conter/retornar uma funçao

function fun3(a,b){
    return function (c){
        console.log(a + b + c)
    }
}
// a primeiro parentesis e a primeira funsao e o segundo e a funçao de primeiro
fun3(5,5)(5)

let somarMais = fun3(8,9)
somarMais(4)



































// ================ Parametros e Retornos - são opcionais =====================

/*
  função (parametro){
     bloco de codigo
     quepode ter um retorno ou nao ter um retorno
  }
*/

function area(largura, altura){ // param sao 'largura' e 'altura'
    let mult = largura * altura
     if(mult >= 20){
        console.log(`valor acima do permitido: ${mult}cm²`)
    }else{
        return (mult + "cm²")
    }
}

console.log(area(2, 20))





























// =================== Parametros Variáveis ============================


function soma(){
    // (argumentos sao os parametros passados), mesmo nao tendo  definido um parametro para nossa funçao, caso usuario colloque um parametro podemos capturalos atraves do comando (arguments).
     let soma = 0
     for(i in arguments){
         soma = soma + arguments[i]
     }
     console.log(soma)
}
// vai somar os numeros e concatenar as strings
soma(2,5,5,10,30)
soma(2,222,"eae")
soma("a","b","c")



































// ===================== Parametro Padrão ============================

// estrategia 1 para gerar um valor padrao
function soma3(a, b, c){
     a = a || 1
     b = b || 1
     c = isNaN(c)? 1 : c // mais seguro e versatil
 
     return a + b + c
}
console.log(soma3(1,3,4), soma3(2,3,))


// estrategia 1 para gerar um valor padrao
function soma9(a = 1,b = 1,c = 1){
     return a + b + c
}
console.log(soma9(3), soma9(3), soma9(3))
console.log(soma9(3), soma9(3), soma9(3))
console.log(soma9(3), soma9(3), soma9(3))
































// ======================== "THIS" pode Variar =========================

// this serve para referenciar o objeto atual de uma execução
// depende da forma que formos usar o 'this' na function ou fora da function


//let f1 = () => {console.log(this === window)} // true
//f1()

// Existe uma maneira de amarrar o 'this' a uma funçao para que ele nao varie, e se chama 'bind'




// ======================== "THIS" e a Função Bind =========================

/*
O método bind() cria uma nova função que, quando chamada, tem sua palavra-chave this definida com o valor fornecido, com uma sequência determinada de argumentos precedendo quaisquer outros que sejam fornecidos quando a nova função é chamada.
*/

// bind so funciona para objetos
let  Pessoa = {
    saudacao: "ola, bom dia",

    andar: "estou andando......",

    falar(){
        console.log(this.saudacao)
                // ou
      //console.log(Pessoa.saudacao)
    },

    Andar(text){
        console.log(this.andar + text)
                // ou
      //console.log(Pessoa.andar + text)
    }
}
Pessoa.falar()

//let falar = Pessoa.falar
//falar() // vai gerar um conflito entre paradigmas: funcional e OO

// usamos o bind para resolver o problema gerado logo acima,
// o "this" sera resolvido para pessoa e vai voltar a apontar para o objeto certo.

let falando = Pessoa.falar.bind(Pessoa)
falando()



Pessoa.Andar("eae como vai?")
// bind resolçveu o problema do 'this' e fez voltar a referenciar-se 
// no mesmo ponto da memoria novamente

// com o 'bind' independentemente onde for disparada a função ela sera executada
let andando = Pessoa.Andar.bind(Pessoa)
andando("bom dia....")

// ========================== TREINANDO ========================================


function cook() {
  console.log(this.ingredients);
}

let dinner = {
  ingredients: "saladoa"
}

let cookBoundToDinner = cook.bind(dinner);
cookBoundToDinner();

// =========================================================================

let calculo = function() {
  return {
    soma: this.soma,
    mult: this.mult,
    div: this.div,
  }
}

let metodos = {
  soma: function(x, y) {
    return x + y;
  },
  mult: function(x, y) {
    return x * y;
  },
  div: function(x, y) {
    return x / y;
  }
}
operacao = calculo.bind(metodos);

console.log(operacao().soma(2,2)); // => 4
console.log(operacao().mult(2,3)); // => 6
console.log(operacao().div(6,3)); // => 2

// ==========================================================================

let oi = function(){
  console.log(this.ola)
}

let saud = {
    ola: "como voce vai? skskskskskks"
}

let saudar = oi.bind(saud)
saudar()

// ==========================================================================

// constando segundos
function Cont(){
      this.segundos = 0

      const self = this // forma de diblar a linguagem
      setInterval(function(){ 
        self.segundos++ 
        console.log(self.segundos)
      }/*.bind(this)*/,1000) // 1000 milisegundos, usamos o 'bind' para arrumar o 'this' e referenciar o numero 
}

//new Cont // instanciando uma funçao
//Cont()
//=================================================================================

// 'setInterval' e uma 'funçao' de temporizador
//setInterval( aqui vai uma function, aqui vai o tempo que ira se repetir )

//setInterval(function(){ numero = 0, numero++, console.log(numero)}, 60000)

//60000 milisegundos = 60 segundos
//1000 milisegundos = 1 segundo
//100000 milisegundos = 100 segundos













































// ============================= Funções Arrow ===============================

// functions Arrows sao funções anonimas,
// para chamar uma function arrow temos que guarda-la dentro de uma variavel.
// variavel = (parametro) => {bloco de codigo}

let dobro = (a) => {
    return 2*a
}

console.log(dobro(2))

//ou

// let dobro = a => 2*a
// console.log(dobro(2)) -> 4


ola = () => "ola"
ola = _ => "ola" // tem parametro, mas nao necessita ser passado, pra isso usamos a '_'.
console.log(ola())


// o 'this' na 'function arrow' e fixo, e o fato da funçao ser chamada de locais diferentes nao influencia o valor do 'this'.

// por exemplo:

 function pesssoa(){
     this.idade = 0

     //const self = this
     setInterval(() => {
      this.idade++
      console.log(this.idade)
     },1000)
}

// new pesssoa ou pesssoa()

//===============================================================


// em uma 'function' o 'this' e global e pode ser apontado para outros obj.
// vamos comparar o parametro passado e ver se e estritamente igual ao this
let comparandoComThis = function(param){
   console.log(this === param)
}

comparandoComThis(global) // => true

const obj = {}
comparandoComThis = comparandoComThis.bind(obj) // this vai apontar para o 'obj', assim deixando de ser global.

comparandoComThis(global) // => false, pois o 'this' esta apontando para o 'obj'
comparandoComThis(obj) // => true, pois usamos a 'função bind', para que, o 'this' referenciar o objeto 'obj'. 

//=========================================================================

// em uma 'function arrow' o this e fixo e so apontara para a 'function arrow'

comparandoComThisArrow = (param) => {console.log(this === param)}

comparandoComThisArrow(this) // => true
comparandoComThisArrow(global) // => false
comparandoComThisArrow(module.exports) // => true

const obj4 = {}
comparandoComThisArrow = comparandoComThisArrow.bind(obj4)

comparandoComThisArrow(obj4) // => false, pois o this em contexto de uma 'function arrow' e fixo e nao pode ser apontado/referenciar outro ponto de memoria.


// OBS: o 'this' aponta para o 'objeto' no qual a função foi escrita.

//==========================================================================

function ola(){
  console.log(this.ola)
}

saudarr = {
  ola: "bom dia amigo"
}

let ola2 = ola.bind(saudar)
console.log(ola2())

//====================================================================

kkk = () => {
   this.lula = "lula e um animal marinho com 4 tentaculos...."

   console.log(this.lula)
}
kkk()


















































// ========================= Funções Anônimas =============================



let somar = function(x = 0, y = 0){
     return x + y
}

let result = function(a=0, b=0, operacao = somar){
      console.log(operacao(a, b))
}
result(6,6) // -> 12
result(2, 3, function(x=0,y=0){return x - y}) // -> -1
result(3,3, (x=0, y=0) => x * y ) // -> 9

// se nao passarmos uma função no terceiro parametro da function result, por padrao a 'operacao' vai receber e executar a 'função somar'

let OLA = {
   falar: function(){
       console.log("bom dia")
   }
}
OLA.falar()

























































// ======================= Funções CallBack =============================

// callback -> Passar uma funçao para outra função e quando um determinado evento disparar pode ocorrer uma ou mais de uma vez

let fabricantes = ["Mercedes", "Audi", "BMW"]

let imprimir = function(nome, indice){
  console.log(`${indice+1}.${nome}`)
}

// forEach( function(nome,indice){bloco de codigo} )

fabricantes.forEach(imprimir)

// passando uma funcao Arrow, de forma direta na function 'forEach' 
fabricantes.forEach( (nome) => console.log(`${nome}`) )

// passando uma funçao anonima, de forma direta na function 'forEach'
fabricantes.forEach( function(nome){
   console.log(`${nome}`)
} )


fabricantes.forEach( function(fabricante){
  console.log(`${fabricante}`)
} )
//ou
// fabricantes.forEach((nome, indice) => {console.log(`${indice+1}.${nome}`)})



// ================== sem callback =============

let nots = [2,5,7,8,8,4,4]

let notasBaixas = []
let notasAltas = []

for(i in nots){
  if(nots[i] < 7){
     notasBaixas.push(nots[i])
  }
}
console.log(notasBaixas)





// ================ com callback ======================
notasBaixas = nots.filter((nota)=>{ return nota < 7})
console.log(notasBaixas)

notasAltas = nots.filter(notas => notas >= 7)
console.log(notasAltas)

// 'filter' e quase igual a foreach, so que a usamos para filtrar e somente com arrays. Aquilo que for verdadeiro sera adicionado altomaticamente a e o que for falso sera descartado.

//======================================================================

// =========== Exemplo de callback no Brownser =============

//document.getElementsByTagName("body")[0].onclick = (e) => {console.log('o evento ocorreu')}

// Nesse exemplo acima, usamos no bownser, estamos pegando todos os elementos com o nome de tag 'body' e acessando o primeiro elemento no indice [0], o primeiro body, e atrelamos no primeiro body um evento 'onclick' que apos ser disparado ira imprimir no console do browser: 'o evento ocorreu'.






















































// ======================== Funções Construtoras ===========================

// usamos uma funçao construtora como usariamos classe em java
// classe nada mais e do que uma forma diferente de escrever uma função em java script

function Carro(velocidadeMaxima = 200, delta = 5){
     // Atributo privado
     let velocidadeAtual = 0 // caso seja privado nao use o 'this'

     // Metodo/ação Publico
     // usamos 'this' para que fique visivel para fora
     this.acelerar = function(){
          if(velocidadeAtual + delta <= velocidadeMaxima){
               velocidadeAtual += delta
          }else{
               velocidadeAtual = velocidadeMaxima 
          }
     }

     // Metodo/ação Publico
     this.getVelocidadeAtual = function(){
          return velocidadeAtual+" Km/s"
     }
}

// para cada dado criado cada um tera sua propria referencia;

let Uno = new Carro // para usarmos a funçao construtora temos que instanciala.

// a cada chamada da function 'acelerar' vamos adicionando +5
Uno.acelerar()


// com a funcao 'getVelocidade()' podemos visualizar a velocidade
console.log(Uno.getVelocidadeAtual())

//==========================================================================

let Ferrari = new Carro(350, 20) // colocamos outras 'velmx' e 'delta'

Ferrari.acelerar()
console.log(Ferrari.getVelocidadeAtual())

















































// ====================== Tipos de Declaração ==========================

console.log(somarr(3,3)) // podemos chamar antes de ser executada

// function declaration
function somarr(x=0, y=0){
   return x+y
}


// function expression
let sub = function(x,y){
    return x-y
}
console.log(sub(3,3)) // so podemos chamar apos sua execução



// named function expression
let mult = function mult(x,y){
   return x*y
}
console.log(mult(3,3)) // mesma ideia da acima














































// =================== constexto Léxico =============================

// e o contexto no qual as coisas foram declaradas na linguagem

const valor = "global"

function minhafuncao(){
      console.log(valor)
}

function exec(){
      const valor = "local"
      minhafuncao()
}

exec()




















// =================== Closures =============================

// Closures e o escopo criado quando uma função e declarada
// esse escopo permite que a função possa acessar e manipular, variaveis externas a função


// Nada mais e do que o contexto léxico em ação

const x = "global"

function fora(){

     const x = "local"

     function dentro(){
         return x
     }

     return dentro()

}
console.log(fora()) // => local, pois retorna uma função dentro, ou seja, local

let myfunction = fora()
console.log(myfunction) // => local



























// ====================== Função Factory =====================================


// funçoes fabrica -> function factory => retorna um objeto

// Factory simples

function criarPessoa(){
    return {
       nome: `Ana`,
       idade: 22,
       sexo: `Fem`
    }
}
console.log(criarPessoa())

//ou


function criarProduto(nome, preco){
    return{
       nome,
       preco,
       desconto: 0.1
    }
}
console.log(criarProduto("notebook", 2000))


/*

os que eu criei para testes alem dos da aula

function criarPessoa(nome = "joao", idade = "23", sexo = "Masc"){
  return {
     nome: `${nome}`,
     idade: idade,
     sexo: `${sexo}`
  }
}
console.log(criarPessoa("jeniffer", 40, "Feminino"))


function criarProduto(nomeProdut, precoProdut){
    return { 
      nome: `${nomeProdut}`, 
      preco: precoProdut.toFixed(2),
      comdesconto: (precoProdut - ((precoProdut * 15)/100) ).toFixed(2)
   }
}

console.log(criarProduto("goiaba", 12.90))
*/










































// ================ Classe VS Function Factory ========================


// classe em js e como uma função
class Pesssoa{
     // definindo os atributos
     constructor(nome, idade){
            this.nome = nome  // tornando o nome publico para usar na classe
            this.idade = idade
     }


     // definindo um Métodos/Ações da classe pessoa com base nos atributos;
     falar(){
         console.log(`Meu nome e ${this.nome}`)
     }

     idadeMinima(){
         if(this.idade > 18){
            console.log(`voce tem ${this.idade} pode passar`)
         }else{
             console.log(`voce tem ${this.idade} espere mais ${18-this.idade} anos para poder passar`)
         }
     }
}
const p1 = new Pesssoa("joao", 21)
p1.falar()
p1.idadeMinima()



// function factory
let Personn = (nome) => {
      return {
         falar: () => {console.log(`eae como vai ${nome}?`)}
      }
}
//const p2 = new Personn("pedro")







// function construtora
function pessoinha(nome = "carlos"){
 
       //atributos
       this.nome = nome

       //metodos
       this.apresentar = () => {
           console.log("Meu nome e " + this.nome)
        }

}
//let p3 = new pessoinha("pedro")
//p3.apresentar()
//console.log(p3.nome) // usando 'this' em nome temos acesso ao nome e podemos alteralo














































// ===================== IIFE (função auto-invocada) =====================

// uma das razoes para usar essa funcao e para fugir do escopo global.

//  IIFE -> Immediately Invocked Function Expression.

(function() {
    console.log("Será Executado na Hora!!!");
    console.log("Foge do escopo Global");
})()




























// ========================= CALL & APPLY ===============================

// 'Call' e 'Apply' sao funcoes, e a unica diferença entre eles e a forma de passar os parametros

function getPreco(imposto = 0, moeda = "R$"){
     return `${moeda} ${this.preco * (1-this.desc) * (1+imposto)}`
}

const Prod1 = {
    nome: "notebook",
    preco: 3500,
    desc: 0.15,
    getPreco
}


//podemos dar os valores de forma global ja que o 'this' no contexto lexico de uma function normal fica global

   global.preco = 20
   global.desc = 0.1
   console.log(getPreco())



// ou podemos acessar pelo obj que ja passa as informaçoes necessarias;
   console.log(Prod1.getPreco())
// poderiamos ter colocado a funcao dentro do obj mas para testar e treinar novas formas colocamos do lado de fora; 


// ====================================================================


const carro = { preco: 4000, desc: 0.10}

console.log(getPreco.call(carro)) // aqui estamos usando a function 'call' para chamar a funcao 'getPreco' executar no obj 'carro' 

// Com call podemos alterar os parametros da function 'getPreco'
console.log(getPreco.call(carro, 0.17, "$"))

console.log(getPreco.call(Prod1, 0.30, "@")) // so testando/treinando

//=====================================================================


// A function 'apply' funciona da mesma forma que a 'call' porem ela aplica a function 'getPreco', nao apenas a chama
console.log(getPreco.apply(carro))

// Podemos mudar os parametros usando o appley tbm porem de forma diferente
console.log(getPreco.apply(carro, [0.17, "$"]))



//podemos chamar pro 'global' ja que tbm tem o 'prec' e o 'desc', por causa do 'this' nos dois, que os torna global
console.log(getPreco.apply(global, [0.10, "EURO"]))

