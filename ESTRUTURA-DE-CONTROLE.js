// ============== Usando Estrutura: IF ==========================
// se acostume a usar funçoes
// estrutura de controle 'if'(se)
// if(condção) {bloco de execuçao}

let result = (nota) => {
    // sempre use chaves para qualquer bloco
    if(nota >= 7){       // se a nota for maior ou igual a 7 imprima: 'aprvado'
       console.log("Aprovado," + " com nota: " + nota)
    }

}

result(7) // se nota nao for menor e nao igual a sete, nao tem saida



// neste exemplo vemos o mal uso do ';' oq acarretou num teste de condição separado de um bloco, que ira executar independentemente do valor boolean da condiçao 'if'
function teste(num){
     if(num > 7); // tome bastante cuidado com ';' para nao fechar errado
     
     {
        console.log(num)
     }
}
// resumo nao use ponto e virgula ate estar acostumado com as condiçoes;
// cuidado com ';', nao use nas estruturas de controle
// vai imprimir os dois:
console.log(6)
console.log(Number.parseInt(8.9))






















// =================== Usando Estrutura: IF/ELSE ==========================

// se(condçao){} senao{}


function eae(numero){
    if(numero >= 7){
      console.log("Aprovado")
    }
    else{
      console.log("Reprovado")
    }
}

eae(2)

























// =================== Usando Estrutura: IF/ELSEIF/ELSE==========================


Number.prototype.entre = function(inicio, fim){
    return this >= inicio && this <= fim 
}

function number(number){
    if(number >= 100){
        console.log("o numero e: " + number  + " maior que 100")
    }
    else if(number >= 40 && number <= 60){
        console.log("o numero e: " + number + " esta entre 40 e 60")
    }
    else if(number.entre(0, 39)){
        console.log("o numero e: " + number + " esta entre 0 e 39")
    }
    else if(number <= -1 && number < 0){
        console.log("o numero e:" + number + " e menor que -1")
    }
}

number(300)
number(55)
number(20)
number(30) 
number(-23)





























// =============== Usando Estrutura: SWITCH-CASE-BREAK-DEFAULT ====================


function cada(op){
     switch (Number(op)){

           case 10: case 9: case 8:
             console.log(`nota: ${op}, quadro de honra A++`)
             break

           case 7: case 6:
             console.log(`nota: ${op}, Aprovado`)
             break

           case 4: case 5: case 3: case 2: case 1:
             console.log(`nota: ${op}, reprovado`)
             break

           default:
             console.log("nota invalida")
             break
    }
}
cada(1)
cada(9)
cada(7)



























// ====================== Usando Estrutura: WHILE ========================


function numAleatorio(min, max){
     let valor = Math.random() * (max-min) + min
     return Math.floor(valor)   //'floor' serve para arredondar o valor
}

//console.log(`valor aleatorio ${numAleatorio(200, 500)}`)


let nume = 0
// enquanto 'nume' for diferente de '7' faça
while( nume != 7){
  nume = numAleatorio(0, 10)

  if(nume == 7){
    console.log("deu seu numero " + nume)    
  }
  else{
    console.log("ainda nao deu seu numero deu numero: " +  nume)
  }
  
}


















// ====================== Usando Estrutura: DO/WHILE ========================

num = 0
//faça e depois teste
do{
  console.log("ola: " + num)
  num = num +1
}while(num <= 7)






















// ====================== Usando Estrutura: FOR =======================


// tabuada
let mult = 0
let numero = 6

console.log("TABUADA do " + numero)
for(let i=0; i <= 10; i++){
     mult = numero * i
     console.log(`${numero} x ${i} = ${mult}`)
}

let contador = 0
let notass = [3,4,5,78,9]
for(let i=0; i < notass.length; i++){
  contador++
  console.log(`${contador}.Nota: ${notass[i]}`)
}



















// ====================== Usando Estrutura: FOR/IN ========================

// nao e que nem um 'for' comum , nao precisamos so i(contador) para fazer as repeticoes e pegar os dados. 

console.log("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
console.log("FOR/IN em Array")
console.log("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
numiro = [2,34,5,555]
// como estamos vendo nao necessitamos criar um contador pois o propruia, variavel 'num' ja faz parte do contador e passa por todos os elementos.
// a variavel criada 'num' ira percorre somente pelos indices
for(let num in numiro){
    if(numiro[num] > 6){
      console.log("Aprovado: " + numiro[num])
    }else{
       console.log("Reprovado: " + numiro[num])
    }
}




let pessoa = {
    nome: "joao",
    idade: 23,
    sexo: "Masculino",
}

console.log("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
console.log("FOR/IN em Objetos")
console.log("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")

for (let atributo in pessoa){
    console.log(atributo + " = " + pessoa[atributo])
} 





 





























// ====================== Usando Break/Continue =================================


// (break/continue) => realiza um desvio de fluxo (funciona bem laços de repetições)

// (break) -> desvia para fora do loop ou switch, break funciona so com loops e swhitches.

// (continue) -> desvia para o inicio do loop, continue e so para loops;

console.log("==================================================================")

let numbers = [1,2,3,4,5,6,7,8,9,10]

for(let x in numbers){
   if(x == 5){ // se for ate o indice 5 saia do bloco
      console.log("saindo do blobo apartir do indice 5")
      break
   }
   console.log("indice = " + x + " =>  valor do indice = " + numbers[x])
}

console.log("=================================================================")

for(let y in numbers){
   if(y == 5){ // se for ate o indice da quinta repeticao, pule e va para a proxima
     console.log("continuando e pulando o incide 5 indo para o 6")
     continue
  }
  console.log(`${y} => ${numbers[y]}`)
}
console.log("fim")

console.log("==============================================================")

externo: for(a in numbers){
         for(b in numbers){
            if(a==2 && b==3){

                console.log("saindo!....")
              
                break externo
                //  Quando queremos sair de um bloco mais externo do que somente sair do princioal usamos um 'rotulo' e o rotulo e o nome do bloco no qual desejamos sair.
              
                // se nao usarmos o rotulo no nesse break ele so saira do loop 'b' e ira continuar no loop 'a' dentro do bloco 'externo'
             }

             console.log(`pares = ${a} , ${b}`)
        }
      }
