// Sentença de Código -> organização do codigo
console.log("ola");

console.log("passo 10");
console.log("passo 4");
console.log("passo 30");

//============================================================================

/*========= COMENTARIOS DE CODIGOS ==========*/

// desabilitar codigos
//console.log("bom dia");

//ou podemos so criar comentarios explicando
//o cod a seguir vai dizer 'hi'
console.log("hi");

/*
 comentario de 
 multiplas 
 linhas 
*/

//============================================================================


/*================= BLOCO DE CODIGO =================*/

{ // abertura do bloco de codigo;

    // isso e um bloco
    console.log("hi");
    console.log("hi");

} // fechamento do bloco de codigo;

// podemos tambem aninhar/agrupar um bloco de codigo;



/* =================== Trabalhando com Dados =================== */

console.log("caneta")
console.log(111) // valor literal/direto;
console.log(10)
console.log(7.4)
console.log(9.0)

let ola = "ola amigoo"
let vedita = "o poder dele e mais de 8mil!!, " + "poder do cara: " + 1000000000
console.log(ola)
console.log(vedita)

let soma = 10 + 10
console.log(soma)

let preco = 19.90
let desconto = 0.4

let precoDesconto = (preco * (1 - desconto))
console.log(precoDesconto)

console.log(typeof(precoDesconto))  // number
console.log(typeof(preco)) // number

let estachovendo = true
console.log(typeof(estachovendo)) // true





