// ================= COMENTÁRIOS DE CÓDIGOS =============================

/*comentario de multiplas linhas*/
//comentario de uma linha











// ================= BÁSICO DE VAR, LET E CONST =========================

// VAR => e global e pode ser remodelada facilmente
// LET => pode alterar o seu valor mas nao remodelar a variavel
// CONST => valor que nao muda, um dado constante

var ola = "bom dia"
var ola = "boa noite" // remodelei a frase
console.log(ola)


let saudacao = "boa noite"
saudacao = "bom dia" // so posso alterar o dado
console.log(saudacao)


const PI = 3.14 // o PI e uma constante que sempre vale '3.14'
console.log(PI)












// ====================== TIPOS EM JS: Number =================================

const peso1 = 1.0

const peso2 = Number("2.0")
console.log(typeof peso2)

console.log(Number.isInteger(peso2)) // verificando se o 'peso2' e um numero inteiro

let avaliacao1 = 9
let avaliacao2 = 7

const total = (avaliacao1 * peso1) + (avaliacao2 * peso2)
const media = total / (peso1 + peso2)

console.log(media.toFixed(2))

console.log(media.toString(2)) // convert para binario

console.log(typeof media)














// ======================== USANDO 'MATH' ===========================


const raio = 5.6
const area = Math.PI * Math.pow(raio, 2) // => raio*raio ou raio²

console.log(area.toFixed(2))

console.log(typeof Math) // Math e um objeto/biblioteca ou seja, guarda um conjunto de funções.












// ====================== TIPOS EM JS: String ===============================


//podemos delimitar strings de tres formas: '', "", ``;

let nome = "cod3er"

console.log(nome.charAt(2)) // vai pegar o caracter na 2 posição

console.log(nome.charCodeAt())

console.log(nome.indexOf("3")) // em que posisao o 3 esta localizado


console.log(nome.substring(2)) // comece da posiçao 2 e va ate a ultima posiçao
console.log(nome.substring(0, 4)) // va do 0 caracter ate caracter na posiçao 4


console.log("escola " + nome + "!") // usamos o sinal de "+" para concatenar


console.log(nome.replace("3", "e")) //trocando o '3' por 'e'

console.log(("joao, pedro, maria").split(',')) 
// a função split() e usada para criar um array, e podemos definir um delimitador para separar os elementos, como: espaço, virgula, :, e etc.....

//OBS: o delimitador de que falo e o que esta entre os elementos, ou seja, oq os separa.

console.log(`ola seja bem vindo ${nome}`) // usando template/crase (``)


//apenas testando
let up = (texto) => texto.toUpperCase()
console.log(`eae.....${up(nome)}!!!!!`)














// ======================== Tipos em JS: Bolean ============================


let isAtivo = false
console.log(isAtivo)

isAtivo = true
console.log(isAtivo)


console.log(nome || "desconhecido")

















// ========================== Tipos em JS: Array ============================

// embora o array aceite qualquer valor, temos que acostumar a criar arrays  homogenizados.

let meuArray = ["ola", true, false, 1, 2.90, 11111]

console.log(meuArray[0]) // Vai imprimir o 'ola' que ta no indice 0
console.log(meuArray[1]) // Vai imprimir o 'true' que ta no indice 1


meuArray.push(true,'bom dia',1.7) // O 'push' Serve para Adicionr Elementos ao Array 


console.log(meuArray.pop()) // O "pop" vai sacar o Último valor do Array, ou seja, ta no indice 5


delete meuArray[1] // usamos 'delete' para deletar um elemento do array.
delete meuArray.pop() // aqui deletamos o ultimo elemento do array 

console.log(meuArray)


console.log(typeof meuArray) // tipo 'object'














// ======================Tipos em JS: Object/Objeto =================================
// objeto e um conjuto de pares formados com uma chave e um valor

// criando um objeto
const prod1 = {
    nome: "Bola de Futebol", // chave(nome) : Valor("Bola de Futebol"),
    preco: 12.50,
}

console.log(prod1.nome) // acessando elemento criado no objeto 'prod1'

prod1.cor = "vermelha" // adicionando um valor ao objeto 'prod1'

console.log(prod1) // inprimindo objeto na tela

prod1['Desconto Legal'] = 0.40 // adicionando um elemento ao objeto 'prod1'

console.log(prod1)


const pessoa = {
    nome: 'João Carlos',
    idade: 18,
    sexo: "Masculino",
    
    localizacao: {          //podemos concatenar os objetos
        cidade: "São Paulo",
        rua: "R. do Camarão",
        numero: 1804
    }
    
}

console.log(pessoa.localizacao.cidade)

console.log(pessoa)

















// ================== Entendendo Null & Undefined =============================

// valores primitivos fazer copias para outros
// valores de objetos sao feitos por referencia e apontam para o mesmo local da memoria




// (Undefined) -> foi declarado mas nao inicializado
// (is not defined) -> nao foi declarado nem inicializado nem atribuido valor algum
// (null) -> nao aponta para nenhum endereço de memoria, sem valor, ausencia de valor. 




let valor1 // vai dar como nao definida 'Undefined'

let valor2 = null // vai dar como vazio  

console.log(valor1)
console.log(valor2)


const obj = {}
//console.log(obj.preco.a) // vai dar erro pois 'preco' nao esta definido e nao podemos pedir algo, indefinido de algo indefinido.























// ===================== EXEMPLOS BÁSICOS DE FUNÇÃO ================================ 

// quase tudo é função

console.log(typeof Object) // do tipo funçao

class produto {}
console.log(typeof produto) // do tipo funçao


// funcoes sao blocos de codigos nomeados que podemos chamalos quando quisermos
// existem funcoes com retorno e sem retorno, funcoes que podem receber dados ou funcoes que nao necessitam receber dados



// funçao com retorno
function soma(n1=0, n2=0){ // n1 e n2 por padrao serao zero
    let soma = n1 + n2;
    return soma;
}
let result = soma(12,4) // chamando a funçao e gurdando o valor em 'result'
console.log(result)




// funçao sem retorno
function soma1(n1=0, n2=0){ // n1 e n2 por padrao serao zero
    let somando = n1 + n2;
    console.log(somando);
}
soma1(12, 10)


// armazenando uma função em uma variavel
const soma2 = function(n1=0, n2=0){
    let soma = n1 + n2
    return soma
}
console.log(soma2(22,200))



// armazenando uma função Arrow em uma variavel
const soma3 = (n1=0, n2=0) => {
    let soma = n1 + n2
    return soma
}
console.log(soma3(30,400))


// retorno implicito
const soma4 = (a=0, b=0) => a + b // ja envia o retorno da soma; em uma linha;

const imprimir2 = a => console.log(a) // so temos um parametro e uma execução;
imprimir2("ola")






























// ================= Declaração de Variaveis com 'VAR' =====================

// escopo  global e função

// VAR e global e pode ser alterada e tranformada em outra variavel
{
    {
        {
            {
                {
                    var ola = "ola" 
                    console.log(ola)
                }
            }
        }    
    }
}

console.log(ola)


function teste(){
    var local = 123
    console.log(local)
}
teste()

//console.log(local)   
// nao conseguimos pegar o 'VAR' dentro de um bloco de função
// ou seja, toda 'VAR' que nao esteja dentro de um bloco de função é global.


var numero = 1
{
    var numero = 2
    console.log(`Dentro = ${numero}`)
}
console.log(`Fora = ${numero}`) // vai imprimir "2", pois vai sobreescrever a anterior


// DICA!! -> fuja do escopo global 






















// ================ Declaração de Variaveis com 'LET' ======================


// ecopo de função, global e bloco

// let ao contrario da var so podemos mudar o seu valor mas nao conseguimos redeclarar a variavel.

let olas = "bom dia"

let bomDia = "hei"

/*
let bomDia = "hallo" 
 
vai dar erro pois a variavel "bomDia" ja foi 
declarada e nao pode ser redeclarada. 
*/


let nnumero = 1 
{
    let numero = 2
    console.log(`Dentro = ${numero}`) // vai imprimir '2'
}
console.log(`Fora = ${numero}`) // vai imprimir '1'
























// ==================== Entendendo Hosting =============================

console.log(a)
var a = 12          // o hosting so acontece com o 'var'




























// =========================== Par: Nome/Valor =========================


const saudac = "bom dia" // contexto lexico 1


function exec(){
    const saudac = "falae" // contexto lexico 2
    console.log(saudac)
}
exec()

const cliente = {
    idade: 12,
    nome: 'joao',
    peso: 90.45,
    sexo: 'M',
    endereco: {
         cidade: 'SP',
         local: 'Av.peixoto',
         numero: 500
    }
}



























// =========================== Notação Ponto ================================

// usamos os pontos para acessar as funçoes das biblitecas por um caminho.
// ou criar nossas proprias bibliotecas e objetos.
console.log(Math.ceil(6.8)) // ceil arredonda

const obijeto = {}
obijeto.nome = "jhon"
//obijeto['nome'] = "joao"
console.log(obijeto.nome)


function obij(){
    // this.myname = myname // o 'nome' ficara visivel para que instanciar a function 'obj' 
     this.exec = function(){
        console.log("fala ae skskskskks")
     }
}

// estamos instanciando a function 'obj' duas vezes com parametros diferentes 
//let obj2 = new obij('joao')
//let obj3 = new obij('eae')

// estamos acessando o conteudo publico/visivel para quem instancia 'this.nome'
//console.log(obj2.myname) 
//console.log(obj3.myname)
//obj3.exec()

































// ==================== Operadores de Atribuição ==================================

/*
  = -> recebe
  == -> igual
  === -> identico
*/

let ain = 7
let bin = 9
let somario = ain + bin
console.log(somario)
/*
// atribuições
b += a // b = b + a

b -= a // b = b - a

b /= 2 // b = b / 2 

b *= a // b = b * a

b %= 2 // b = b % 2

*/































// =================== Operadores: Destructuring =============================

// destructuring serve para pegar atributos do objeto de forma simples

// usando destructuring em um objeto
const pessoa2 = {
    noome: "ana",
    idade: 5,
    endereco: {
        logradouro: 'Rua ABC',
        numero: 2033
    }
}

const { noome, idade } = pessoa2 // tire de dentro do objeto 'pessoa2' os atributos 'nome' e 'idade';

console.log(noome, idade)


// outra forma de fazer 


const { noome: n, idade: i } = pessoa2 // tire de dentro do objeto 'pessoa2' os atributos 'nome' e 'idade';
// alem de  extrair os atributos de nome e idade estamos guardando eles em variaveis;

console.log(n, i)


// acessando 'logradouro' e 'numero' e adicionando 'cep' dentro de endereços
const { endereco: { logradouro, numero: o, cep = "900000-33" } } = pessoa2
// atribuimos um valor ao 'cep' para que nao desse como 'undefined'


console.log(logradouro, o, cep)



//======================================================================


//usando destructuring em array

const [r] = [10] // criando um valor/posiçao rm uma array e atribuindo o valor '10' 
console.log(r)

const [ n1, n2, n3, n4, n5 = 50 ] = [10, 20, 30, 40]
// no primeiro array criamos variaveis e no segundo atribuimos o valor a cada um 
console.log(n1, n2,n3,n4,n5)

const [,[,nota]] = [[4,5,6], [2, 90, 12]]
console.log(nota) // vai imprimir o valor '90'

//===============================================================

function rand({min=0, max=1000}){
    let valor = Math.random() * (max - min) + min
    return Math.floor(valor)
}

let obj4 = {min: 30, max: 60} // podemos utilizar um objeto para passar os valores min e max 

console.log(rand({min: 100, max: 300})) // vai ir entre 100 e 300
console.log(rand({})) // vai ir so pelo padrao 0 e 1000
console.log(rand(obj4)) // nao precisa das chaves pois, por ser um objeto ja tem chaves e valores.
console.log(rand({min: 500})) // vai entre 500 ate 1000.




function rand2([min = 0, max = 1000]){
     if(min > max) [min, max] = [max, min] // trocando valores caso o minimo for maior que o maximo, ai o  minimo vira o maximo e o maximo vira o minimo

     let valor = Math.random() * (max - min) + min
     return Math.floor(valor)
}

console.log(rand2([300, 500])) // nao vai precisar da troca
console.log(rand2([500, 100])) // vai realizar a troca
console.log(rand2([,300]))

































// ======================= Operadores: Aritimeticos ============================

// const [a, b, c, d, e] = [1,2,3,4,5]

// ordem de precedencia
// ()
// * ,  / , %
// +, -
/*
let eae = -a + b // -1 + 2 = 1
let somali = a + b + c + d + e
let modulo_resto_da_divisao = b % 2 
*/




























// ======================= Operadores: Relacionais ============================

// so tem duas saidas em operadores relacionais 'true' ou 'false'

/*
 '>' = maior que
 '<' = menor que
 '>=' =maior ou igual
 '<=' = menor ou igual
 '==' = igual
 '!' = diferente
 '===' = indentico 
 '!==' = estritamente diferente
*/

console.log("1" == 1) // true
console.log("3" === 3) // false
console.log(undefined == null) // true
console.log(undefined === null) // false
































// ======================= Operadores: Lógicos ============================


/*
ou -> || -> so precisa de uma teste relacional que seja true
 ex:  v ou v -> true
      v ou f -> true
      f ou v -> true
      f ou f -> false

e -> && -> precisa que os dois testes relacionais sejam true pra dar true
 ex:  v e v -> true
      v e f -> false
      f e v -> false
      f e f -> false

nao -> ! -> ao conrtrario: ex oq nao e true e false e assim vise versa
 ex:   not true -> false  / oq nao e true e false
       not false -> true  / oq nao e false e true

xor -> no 'xor' tem que ser diferente  se for igual e falso
 ex:  v xor v -> false
      v xor f -> true
      f xor v -> true
      f xor f -> false


*/ 









































// ======================= Operadores: Unários ============================


num1 = 3
num2 = 1
// ============ Incrementações: ===========
  console.log(++num1) // num1 = num1 + 1
  console.log(num2--) // num2 = num2 - 1
  console.log(num1++) // num1 = num1 + 1 
  console.log(--num2) // num2 = num2 - 1
// ========================================




 




























// ======================= Operador: Ternário ============================


// (teste da condiçõa) ? verdadeiro : falso
let notass = 8
notass >= 7 ? console.log("Aprovado") : console.log("Reprovado")


// aqui eu utilizei uma Function 'Arrow'; (parametro) => {bloco de execução}
const resultadp = (notas) => {notas >= 7 ? 'aprovado' : 'reprovado'} 
console.log(resultadp(4)) // reprovado
console.log(resultadp(8)) // aprovado












































// ========== Contexto de Execução: Browser VS Node ==========


// Execução do js no Browser:  (Front-End)
// Execução do js no Node.js:  (Back-End)


// this e global 





































// ========== Tratamento de Erro: (try/catch/throw) ==========



function error(e){
    // throw e usado quando da erros e, é uma forma de saida que podemos definir:
    
    // podemos usar o 'throw' de varias formas:
    //throw new Error(".......") // vai alterar a saida de erro
    //throw 10
    //throw true
    throw 'ocorreu um erro inesperado'
}


function Gritar(obj5){
  try { // tente executar, 
    console.log(obj5.nome.toUpperCase() + "!!!!")
  } 
  catch (e){ // se nao executar e der erro vai parar no bloco 'catch'
     console.log(error(e))
     //throw new Error("Mjudei a saida de erro")
     //console.log("Deu RUim")
  }
  finally{ // nao importa se o bloco deu erro ou nao, vai executar igual
     console.log("......Final!!!!")
  }
}



const obj5 = { nome: "joao" }
Gritar(obj5) // recebendo o objeto5 como parametro

error()

